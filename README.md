# dashboard-rbui

Dashboard RBuI is a dashboard for all your web application.

## Getting started

It will probably be necessary to change the permissions to upload folders/

1) Run the following command in your host:

`$ sudo chmod 777 uploads`

2) If you want the database to be locally, set the DOCKERIZED env variable as true. Otherwise, set ATLAS_MONGODB_CONNECTION_STRING env variable with the string provided by mongo db. 

3) Then just initialize the container

`$ docker compose up -d`

