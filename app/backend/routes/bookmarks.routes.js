const router = require('express').Router();
const BookmarksController = require('../src/controllers/bookmarks.controller')
const multer = require('multer');

const upload = multer({
    storage:  multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'uploads/')
        },
        filename: function (req, file, cb) {
            const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
            const ext = file.mimetype.split("/")[1];
            cb(null, uniqueSuffix + Date.now() + '.' + ext)
        }
    }),
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpg' && file.mimetype !== 'image/jpeg')
            return cb(new Error('Invalid mimetype'));
        cb(null, true)
    }
});

const uploadSingleImage = upload.single('imgLink');

router.post('/add', async (req, res) => {
    uploadSingleImage(req, res, async function (err) {
        if (err)
            return res.status(400).send({message: err.message})

        req.body.imgLink = `uploads/${req.file.filename}`;
        const controllerHandler = new BookmarksController(req);
        const result = await controllerHandler.add();
        return (result.success === true)
            ? res.json({id: result.id, success: true})
            : res.status(500).json({error: result.msg})
    })
})

router.get('/get-bookmarks-by-tag/:tagName', async (req, res) => {
    const controllerHandler = new BookmarksController(req);
    const result = await controllerHandler.getBookmarksByTag();
    return res.json(result);
})

router.post('/delete', async (req, res) => {
    const controllerHandler = new BookmarksController(req);
    const result = await controllerHandler.delete()
    return res.json(result);
});

router.post('/update', async (req, res) => {
    const controllerHandler = new BookmarksController(req);
    const result = await controllerHandler.update()
    return res.json(result);
})

module.exports = router;