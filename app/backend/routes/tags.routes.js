const router = require('express').Router();
const TagsController = require('../src/controllers/tags.controller')

router.post('/add', async (req, res) => {
    console.log('route>', '/add');
    const controllerHandler = new TagsController(req);
    const result = await controllerHandler.add();

     return (result.success === true)
         ? res.json({id: result.id, success: result.success})
         : res.status(500).json({error: result.msg})
})

router.get('/get/all', async (req, res) => {
    console.log('route>', '/get/all');
    const controllerHandler = new TagsController(req);
    const result = await controllerHandler.getAll();
    return res.json(result);
})

router.post('/delete', async (req, res) => {
    console.log('route>', '/delete');
    const controllerHandler = new TagsController(req);
    const result = await controllerHandler.delete()
    return res.json(result);
});

router.post('/update', async (req, res) => {
    console.log('route>', '/update');
    const controllerHandler = new TagsController(req);
    const result = await controllerHandler.update()
    return res.json(result);
})

module.exports = router;