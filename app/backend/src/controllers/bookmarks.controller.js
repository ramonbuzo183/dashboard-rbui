const BookmarksModel = require('../models/Bookmarks.model')
const path = require('path')
const fs = require('fs');

BookmarksController = class BookmarksController {
    constructor(req) {
        this.req = req;
    }

    async add() {
        let success = false, msg, id = null;
        try {
            console.log(this.req.body)
            let {name, description, link, imgLink, tagsAssigned} = this.req.body;

            if (!name)
                return {id: null, success: false, msg:  'missing `name` param'}

            const existingBookmark = await BookmarksModel.findOne({name: name})

            if (existingBookmark)
                return {id: null, success: false, msg:  'bookmark name already exists'}

            const newBookmark = new BookmarksModel({
                name: name,
                description: description,
                link: link,
                imgLink: imgLink,
                tagsAssigned: tagsAssigned //it is an array []
            });

            const savedBookmark = await newBookmark.save();
            success = true;
            msg = "success";
            id = savedBookmark._id
        } catch (e) {
            msg = e.message;
        }
        return {
            id: id,
            success: success,
            msg: msg
        }
    }

    async getBookmarksByTag() {
        let tagToFind = this.req.params.tagName
            .replaceAll('+', ' ')
            .replaceAll('%20t', ' ');

        let bookmarksFound = [];

        const checkTag = obj => obj.name === tagToFind

        await BookmarksModel.find().sort({position_bk: 1})
            .populate('tagsAssigned')
            .then(bookmarks => {
                bookmarks.forEach(bookmark => {
                    if (bookmark.tagsAssigned.some(checkTag)) {
                        let imgUrl = process.env.DOCKERIZED === 'false' ? `http://localhost:8989/${bookmark.imgLink}` : `/${bookmark.imgLink}`
                        bookmarksFound.push(
                            {
                                _id: bookmark._id,
                                name: bookmark.name,
                                description: bookmark.description,
                                link: bookmark.link,
                                imgLink: imgUrl,
                                tagDetected: tagToFind,
                                order: bookmark.position_bk
                            });
                    }
                })
            });
        return bookmarksFound;
    }

    async delete(id) {
        id = (typeof id !== 'undefined') ? id : this.req.body.id;

        if (!id)
            return {success: false, msg:  'unexpected error - delete function does not how to handle the request'}

        let result;
        try {
            const deleteTag = await BookmarksModel.deleteOne({_id: id})
            console.log(deleteTag);

            result =  deleteTag.deletedCount  === 1
                ? {success: true, msg: 'success'}
                : {success: false, msg: 'record doesn\'t exist or already deleted'}
        } catch (e) {
            result = {success: false, msg: e.message}
        }
        return {success: result.success, msg: result.msg}
    }



    async deleteBookmarksByTagId(tagId)
    {
        const checkTag = obj => obj.id === tagId

        await BookmarksModel.find().sort({position_bk: 1})
            .populate('tagsAssigned')
            .then(bookmarks => {
                bookmarks.forEach(bookmark => {
                    if (bookmark.tagsAssigned.some(checkTag) === true) {
                        this.delete(bookmark.id).then((result) => {
                            console.log(result);
                            if (result.success === true) {
                                console.log ('trying to remove the asset from server');
                                try{fs.unlinkSync(path.join(__dirname, "../../", bookmark.imgLink));} catch (e) {}
                            }
                        })
                    }
                })
            });
    }

    async update() {
        let {id, name, description, link, tagsAssigned, position} = this.req.body;

        if (!id)
            return {success: false, msg:  'unexpected error - update function does not how to handle the request'}

        let payload = {};
        let result;

        if (name !== undefined)
            payload.name = name;

        if (link !== undefined)
            payload.link = link;

        if (position !== undefined)
            payload.position_bk = position;

        if (description !== undefined)
            payload.description = description;

        if (tagsAssigned !== undefined)
            payload.tagsAssigned = tagsAssigned;

        try {
            const updatedBookmark = await BookmarksModel.findByIdAndUpdate(id , payload)

            let updated = false;

            if (name !== undefined)
                updated = updatedBookmark.name !== name;
            else if (link !== undefined)
                updated = updatedBookmark.link !== link;
            else if (position !== undefined)
                updated = updatedBookmark.position_bk !== position;
            else if (description !== undefined)
                updated = updatedBookmark.description !== description;
            else if (tagsAssigned !== undefined)
                updated = updatedBookmark.tagsAssigned !== tagsAssigned;

            result = updated  === true
                ? {success: true, msg: 'success'}
                : {success: false, msg: 'value is already updated. No changes were made'}
        } catch (e) {
            result = {success: false, msg: e.message}
        }
        return {success: result.success, msg: result.msg}
    }
}

module.exports = BookmarksController;