const TagsModel = require('../models/Tags.model')
const BookmarksController = require('../controllers/bookmarks.controller')

TagsController = class TagsController {
    constructor(req) {
        this.req = req;
    }

    async add() {
        let success = false, msg = null, id = null;
        try {
            let {name, description} = this.req.body;
            description = !description ? '' : description;

            if (!name)
                return {id: null, success: false, msg:  'missing `name` param'}

            const existingTag = await TagsModel.findOne({name: name})

            if (existingTag)
                return {id: null, success: false, msg:  'tag name already exists'}

            const newTag = new TagsModel({
                name,
                description
            })

            const savedTag = await newTag.save();

            success = true;
            msg = "success";
            id = savedTag._id
        } catch (e) {
            msg = e.message;
        }

        return {
            id: id,
            success: success,
            msg: msg
        }
    }

    async getAll() {
        let tagsMap = [];

        const tags = await TagsModel.find({}).sort({position: 1}) //1 for ascending and -1 descending;

        tags.forEach((tag) => {
            tagsMap.push({id: tag._id, name: tag.name, description: tag.description, order: tag.position});
        })
        return tagsMap;
    }

    async delete() {
        let {id, name} = this.req.body;

        if (!id && !name)
            return {success: false, msg:  'unexpected error - delete function does not how to handle the request'}

        const payload = id ? {_id: id} : {name: name};

        let result;
        try {
            const controllerHandler = new BookmarksController(this.req);
            const bookmarksDeleted = await controllerHandler.deleteBookmarksByTagId(id);

            const deleteTag = await TagsModel.deleteOne(payload)
            result =  deleteTag.deletedCount  === 1
                ? {success: true, msg: 'success'}
                : {success: false, msg: 'record doesn\'t exist or already deleted'}
        } catch (e) {
            result = {success: false, msg: e.message}
        }
        return {success: result.success, msg: result.msg}
    }

    async update() {
        let {id, name, description,  position} = this.req.body;

        if (!id)
            return {success: false, msg:  'unexpected error - update function does not how to handle the request'}

        let result;
        let payload = {};

        if (name !== undefined)
            payload.name = name;

        if (position !== undefined)
            payload.position = position;

        if (description !== undefined)
            payload.description = description;

        try {
            const updateTag = await TagsModel.findByIdAndUpdate(id, payload)

            let updated = false;

            if (position !== undefined)
                updated = updateTag.position !== position;
            else if (name !== undefined)
                updated = updateTag.name !== position;
            else if (description !== undefined)
                updated = updateTag.description !== description;

            result = updated  === true
                ? {success: true, msg: 'success'}
                : {success: false, msg: 'value is already updated. No changes were made'}
        } catch (e) {
            result = {success: false, msg: e.message}
        }
        return {success: result.success, msg: result.msg}
    }
}

module.exports = TagsController;