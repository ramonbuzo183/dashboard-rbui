const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);

const Schema = mongoose.Schema;

const schemaName = 'Tags';

const tagsSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false,
        minLength: 0
    },
    position: {
        type: Number
    }
});
tagsSchema.plugin(AutoIncrement, {inc_field: 'position'})

const TagsModel = mongoose.model(schemaName, tagsSchema)
module.exports = TagsModel;