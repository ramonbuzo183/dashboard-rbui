const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);

const Schema = mongoose.Schema;

const schemaName = 'Bookmarks';

const bookmarksSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {type: String},
    link: {
        type: String,
        required: true,
    },
    imgLink: {type: String},
    tagsAssigned: [{ type: Schema.Types.ObjectId, ref: 'Tags' }],
});

bookmarksSchema.plugin(AutoIncrement, {inc_field: 'position_bk'})
const BookmarksModel = mongoose.model(schemaName, bookmarksSchema)

module.exports = BookmarksModel;