const express = require("express");
const cors = require('cors')
const path = require('path')
const mongoose = require('mongoose');

require('dotenv').config({path: `${__dirname}/.env`});

const app = express()
app.use(express.static(path.join(__dirname, "../frontend/build")))
app.use(express.json());
app.use(cors());

let mongoUrl = '';
if (process.env.DOCKERIZED === 'true') {
    const dbPassword = process.env.ME_CONFIG_MONGODB_ADMINPASSWORD;
    const dbUserName = process.env.ME_CONFIG_MONGODB_ADMINUSERNAME;
    const dbPort = process.env.ME_CONFIG_MONGODB_PORT;
    mongoUrl = `mongodb://${dbUserName}:${dbPassword}@localhost:${dbPort}`
} else if (process.env.ATLAS_MONGODB_CONNECTION_STRING){
    mongoUrl = process.env.ATLAS_MONGODB_CONNECTION_STRING;
} else {
    throw new Error('No database configuration was found.');
}

const PORT = process.env.APP_PORT || 8989;

console.log('myCustomMongoUrl', mongoUrl);

app.listen(PORT, () => {
    console.log(`The server has started on port ${PORT}`)
    }
)

mongoose.connect(mongoUrl, {}, (err) => {
    if (err) throw err
    console.log(`MongoDB connection established`)
})

app.get(['/', '/tag/:tagName/bookmarks'], (req, res) => {
    res.sendFile(path.join(__dirname, "../frontend/build", 'index.html'));
})

app.get('/uploads/:filename', (req, res) => {
    res.sendFile(path.join(__dirname, "/uploads", req.params.filename));
})

app.use('/tags', require('./routes/tags.routes'));
app.use('/bookmarks', require('./routes/bookmarks.routes'));
