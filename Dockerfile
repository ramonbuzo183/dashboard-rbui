FROM node:lts-alpine

COPY app/backend/ /app/backend
COPY app/frontend /app/frontend

WORKDIR /app/backend
RUN npm install
CMD ["npm", "start"]